import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

//import { registerApplication, start } from 'single-spa'


Vue.config.productionTip = false

// function createScript(url) {
//   return new Promise((resolve, reject) => {
//     const script = document.createElement('script')
//     script.src = url
//     script.onload = resolve
//     script.onerror = reject
//     const firstScript = document.getElementsByTagName('script')[0]
//     firstScript.parentNode.insertBefore(script, firstScript)
//   })
// }

// function loadApp(url, globalVar) {
//   return async () => {
//     await createScript(url + '/js/chunk-vendors.js')
//     await createScript(url + '/js/app.js')
//     return window[globalVar]
//   }
// }

// const apps = [
//   {
//     name: 'memoriales',
//     app: loadApp('http://localhost:8081', 'memoriales'),
//     activeWhen: location => location.pathname.startsWith('/memoriales'),
//     customProps: {authToken: 'Token desde principal', ejemplo:'Ejemplo de usuario desde principal' }
//   },
//   {
//     name: 'causas',
//     app: loadApp('http://localhost:8082', 'causas'),
//     activeWhen: location => location.pathname.startsWith('/causas'),
//     customProps: {}
//   }
// ]

// for (let i = apps.length - 1; i >= 0; i--) {
//   registerApplication(apps[i])
// }

new Vue({
  router,
  store,
  vuetify,
  // mounted() {
    
  //   start()
  // },
  render: h => h(App)
}).$mount('#app')
