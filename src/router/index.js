import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    name:'Menu',
    path: '/',
    component: () => import('../views/dashboard/options/Iniciomenu.vue'),
    children:[
      // {
      //   name: 'Menuprincipal',
      //   path: '',
      //   component: () => import('../views/dashboard/options/Iniciomenu.vue'),
      // },
      
      // {
      //   name: 'Menu2',
      //   path: '/dashboard/menu2',
      //   component: () => import('../views/dashboard/options/Menu2.vue'),
      // },
    ]
  },
  {
    name: 'Login',
    path: '/login',
    component: () => import('../views/login/login.vue'),
  },
  {
    name: 'Menu2',
    path: '/dashboard/menu2',
    component: () => import('../views/dashboard/options/Menu2.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
